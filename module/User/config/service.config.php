<?php

namespace User;

use Auth\Adapter\Authentication as AuthAdapter;
use Perfil\Service\Perfil as PerfilService;
use Perfil\Service\Address as AddressService;
use Perfil\Form\Address as AddressForm;
use Auth\Form\Login as LoginForm;
use Auth\Form\Filter\Login as LoginFilter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Base\Mail\Mail;
use Zend\Stdlib\Hydrator\ClassMethods;

return array(
    'factories' => [
        //Authentication
        'Authentication' => function () {
                //Cria storage para gravar sessão de autenticação
                //Define a SessionStorage para Auth
                $auth = new AuthenticationService();
                $auth->setStorage(new Session('User'));

                return $auth;
            },
        //Storage\Session
        'Session' => function () {
                return new Session('User');
            },
        //Mail
        __NAMESPACE__ . '\Mail\Transport' => function (ServiceLocatorInterface $sm) {
                //pega configurações de global.php
                $config = $sm->get('Config');
                $transport = new Smtp();

                //seta todas as opções
                $options = new SmtpOptions($config['mail']);
                $transport->setOptions($options);

                return $transport;
            },
        __NAMESPACE__ . '\Mail\Mail' => function (ServiceLocatorInterface $sm) {
                return new Mail(
                    $sm->get('User\Mail\Transport'),
                    $sm->get('View'),
                    'add-user'
                );
            },
        //User
        __NAMESPACE__ . '\Service\User' => function (ServiceLocatorInterface $sm) {
                return new Service\User(
                    $sm->get('Doctrine\ORM\EntityManager'),
                    $sm->get('User\Mail\Mail')
                );
            },
        //NameSpace Auth
        //Auth
        'Auth\Adapter\Authentication' => function (ServiceLocatorInterface $sm) {
                return new AuthAdapter($sm->get('Doctrine\ORM\EntityManager'), 'User\Entity\User');
            },
        'Auth\Form\Login' => function () {
                $filter = new LoginFilter();

                return new LoginForm('login', $filter);
            },
        //Namespace Perfil
        //Perfil
        'Perfil\Service\Perfil' => function (ServiceLocatorInterface $sm) {
                return new PerfilService($sm->get('Doctrine\ORM\EntityManager'));
            },
        'Perfil\Form\Address' => function (ServiceLocatorInterface $sm) {
                $hydrator = new ClassMethods(false);

                //Add Id do Usuário logado na Entity de Address
                $auth = $sm->get('Authentication');
                $idUser = $auth->getIdentity()['user']->getId();

                $em = $sm->get('Doctrine\ORM\EntityManager');
                $user = $em->getReference('User\Entity\User', $idUser);

                $entityAddress = $sm->get('User\Entity\Address');
                $entityAddress->setUser($user);

                //Retorna Instância
                return new AddressForm(
                    'address',
                    $hydrator,
                    $entityAddress,
                    $sm->get('Perfil\Form\Filter\Address')
                );
            },
        //Address
        'Perfil\Service\Address' => function (ServiceLocatorInterface $sm) {
                return new AddressService($sm->get('Doctrine\ORM\EntityManager'));
            },
    ],
    'invokables' => array(
        __NAMESPACE__ . '\Entity\User' => 'User\Entity\User',
        __NAMESPACE__ . '\Entity\Address' => 'User\Entity\Address',
        //Authentication
        'Auth' => 'Zend\Authentication\AuthenticationService',
        'Perfil\Form\Filter\Address' => 'Perfil\Form\Filter\Address'
    ),
);