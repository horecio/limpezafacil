<?php
namespace User;
return array(
    'router' => array(
        'routes' => array(
            'user' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/user[/:action][/:id]',
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '\d+',
                    ),
                    'defaults' => array(
                        'controller' => 'user',
                        'action' => 'index',
                    ),
                ),
            ),
            'user-perfil' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/perfil',
                    'defaults' => array(
                        'controller' => 'perfil',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'address' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/endereco[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+',
                            ),
                            'defaults' => array(
                                'controller' => 'address-perfil',
                                'action' => 'edit'
                            ),
                        ),
                    ),
                    'user' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/usuario[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'user-perfil',
                                'action' => 'edit'
                            ),
                        ),
                    ),
                ),
            ),
            //Ativação via email
            'user-activate' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/register/activate[/:key]',
                    'defaults' => array(
                        'controller'    => 'user',
                        'action'        => 'activate',
                    ),
                ),
            ),
            //Authentication
            'user-auth' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/auth',
                    'defaults' => array(
                        'controller'    => 'auth',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:action]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array(
                                'controller'    => 'auth',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            //NameSpace User
            'user' => 'User\Controller\UserController',
            //NameSpace Auth
            'auth' => 'Auth\Controller\AuthController',
            'perfil' => 'Perfil\Controller\PerfilController',
            //NameSpace Perfil
            'user-perfil' => 'Perfil\Controller\UserController',
            'address-perfil' => 'Perfil\Controller\AddressController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => 'application_entities'
                ),
            ),
        ),
    ),
);
