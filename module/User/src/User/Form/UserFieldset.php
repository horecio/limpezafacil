<?php
namespace User\Form;

use Base\Entity\AbstractEntity;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class UserFieldset extends Fieldset implements InputFilterProviderInterface, ObjectManagerAwareInterface
{
    protected $name;
    protected $hydrator;
    protected $entity;
    protected $objectManager;

    function __construct
    (
        $name,
        AbstractHydrator $hydrator,
        AbstractEntity $entity
    )
    {
        parent::__construct($name);

        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setAttribute('method', 'post');
    }
    public function init()
    {
        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'NOME',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'dataNascimento',
            'type' => 'Date',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'DATA DE NASCIMENTO',
                'class' => 'form-control',
                'format' => 'd/m/Y'
            )
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Email',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'EMAIL',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'passwd',
            'type' => 'Password',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'SENHA',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'confirmation',
            'type' => 'Password',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'REDIGITE SUA SENHA',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cpf',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'CPF',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'operadora',
            'type' => 'Select',
            'options' => array(
                'disable_inarray_validator' => true,
                'empty_option' => 'Operadora',
                'value_options' => array(
                    'oi' => 'Oi',
                    'vivo' => 'Vivo',
                    'claro' => 'Claro',
                    'Tim' => 'Tim',
                ),
            ),
            'attributes' => array(
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'celular',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'CELULAR',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'fixo',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'TELEFONE FIXO',
                'class' => 'form-control',
            ),
        ));
        $roles = $this->getObjectManager()
            ->getRepository('Acl\Entity\Role')
            ->fetchNames();
        $this->add(array(
                'type' => 'Select',
                'name' => 'perfil',
                'options' => array(
                    'value_options' => $roles
                ),
            'attributes' => array(
                'class' => 'form-control',
            ),
            ));
    }

    /**
     * @return array
    \*/
    public function getInputFilterSpecification()
    {
        return array(
            'name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'Não pode estar em branco',

                            )
                        ),
                    )
                )
            ),
            'dataNascimento' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'email' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'Por favor, digite o seu email'
                            )
                        ),
                    ),
                    array(
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/',
                            'messages' => array(
                                'regexNotMatch' => 'Seu email está estranho. Tente novamente'
                            )
                        )
                    )
                ),
            ),
            'passwd' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Não pode estar em branco')
                        )
                    )
                )
            ),
            'confirmation' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array('name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Não pode estar em branco')
                        ),
                    ),
                    array('name' => 'Identical',
                        'options' => array(
                            'token' => 'passwd',
                            'messages' => array('notSame' => 'Acho que você redigitou a SENHA errada')
                        )
                    )
                )
            ),
            'cpf' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array('name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Não pode estar em branco')
                        )
                    )
                )
            ),
            'operadora' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array('name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Não pode estar em branco')
                        )
                    )
                )
            ),
            'celular' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array('name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Não pode estar em branco')
                        )
                    )
                )
            ),
            'fixo' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'perfil' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )
        );
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }
}