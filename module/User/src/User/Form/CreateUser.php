<?php
namespace User\Form;

use Zend\Form\Form;

class CreateUser extends Form
{
    public function init()
    {
        $this->add(array(
            'name' => 'address',
            'type' => 'User\Form\AddressFieldset',
            'options' => array(
                'use_as_base_fieldset' => true
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
            ),
        ));
    }
}