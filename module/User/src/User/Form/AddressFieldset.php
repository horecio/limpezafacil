<?php
namespace User\Form;

use Base\Entity\AbstractEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class AddressFieldset extends Fieldset implements InputFilterProviderInterface
{
    protected $name;
    protected $hydrator;
    protected $entity;
    protected $filter;

    function __construct
    (
        $name,
        AbstractHydrator $hydrator,
        AbstractEntity $entity
    )
    {
        parent::__construct($name);
        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setAttribute('method', 'post');
    }
    public function init()
    {
        $this->add(array(
            'name' => 'logradouro',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'ENDEREÇO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cep',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'CEP',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'bairro',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'BAIRRO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cidade',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'value' => 'GURUPI',
                'class' => 'form-control',
                'disabled' => 'disable'
            ),
        ));
        $this->add(array(
            'name' => 'estado',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'value' => 'TO',
                'class' => 'form-control',
                'disabled' => 'disable'
            ),
        ));
        $this->add(array(
            'name' => 'pais',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'value' => 'Brasil',
                'class' => 'form-control',
                'disabled' => 'disable'
            ),
        ));
        $this->add(array(
            'name' => 'numero',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'N°',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'complemento',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'COMPLEMENTO (Ex.: Quadra..., Portão branco..., Em frente a...)',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'user',
            'type' => 'User\Form\UserFieldset',
            'options' => array(
                'label' => 'Usuário'
            )
        ));
    }
    /**
     * @return array
    \*/
    public function getInputFilterSpecification()
    {
        return array(
            'logradouro' => array(
                'required' => true,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'=>'NotEmpty',
                        'options'=>array(
                            'messages'=>array(
                                'isEmpty'=>'Não pode estar em branco')
                        )
                    )
                )
            ),
            'cep' => array(
                'required' => false,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
            ),
            'numero' => array(
                'required' => true,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'=>'NotEmpty',
                        'options'=>array(
                            'messages'=>array(
                                'isEmpty'=>'Não pode estar em branco')
                        )
                    )
                )
            ),
            'bairro' => array(
                'required' => true,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'=>'NotEmpty',
                        'options'=>array(
                            'messages'=>array(
                                'isEmpty'=>'Não pode estar em branco')
                        )
                    )
                )
            ),
            'complemento' => array(
                'required' => true,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'=>'NotEmpty',
                        'options'=>array(
                            'messages'=>array(
                                'isEmpty'=>'Ajude-nos a encontrá-lo preencha este campo')
                        )
                    )
                )
            ),
            'cidade' => array(
                'required' => false,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
            ),
            'estado' => array(
                'required' => false,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
            ),
            'pais' => array(
                'required' => false,
                'filters'  => array(
                    array('name'=>'StripTags'),
                    array('name'=>'StringTrim'),
                ),
            )
        );
    }
}