<?php
namespace User\Service;

use Base\Mail\Mail;
use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class User extends AbstractService
{
    protected $entityAddress;
    protected $userMail;

    function __construct(EntityManager $entityManager = null, Mail $userMail)
    {
        parent::__construct($entityManager);
        $this->entity = 'User\Entity\User';
        $this->entityAddress = 'User\Entity\Address';
        $this->userMail = $userMail;
    }

    /*
     * Insere Usuário e envia email
     */
    public function insert($entity)
    {
        $entity = parent::insert($entity);
        $dataEmail = array(
            'nome' => $entity->getUser()->getName(),
            'activationKey' => $entity->getUser()->getActivationKey()
        );
        if($entity){
            $this->userMail
                ->setSubject('Confirmação de Cadastro -LimpezaFacil.com')
                ->setTo($entity->getUser()->getEmail())
                ->setData($dataEmail)
                ->prepare()
                ->send();

            return $entity;
        }
    }

    /*
     * Delete entity Address depois User
     */
    public function delete($id)
    {
        if (!is_int($id)) {
            $messageException = 'Id deve ser um número inteiro';
            throw new \InvalidArgumentException($messageException);
        }

        if ($id <= 0) {
            $messageException = 'Id deve ser maior do que zero';
            throw new \OutOfBoundsException($messageException);
        }

        $entityUser = $this->getEm()
            ->getReference($this->entity, $id);

        $entityAddress = $this->getEm()
            ->getRepository($this->entityAddress)
            ->findOneByUser($entityUser);

        $this->getEm()->remove($entityAddress);
        $this->getEm()->flush();

        //parent::delete($entityAddress->getUserId());

        return $id;
    }

    //Via email
    public function activate($key)
    {
        $repo = $this->getEm()
            ->getRepository($this->entity);

        $user = $repo->findOneByActivationKey($key);

        if($user && !$user->getStatus()) {
            $user->setStatus(true);

            $this->getEm()->persist($user);
            $this->getEm()->flush();

            return $user;
        }
    }
}