<?php
namespace User\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class UserController extends AbstractController
{
    protected $entityAddress;
    protected $userMail;

    public function __construct()
    {
        $this->entity = 'User\Entity\User';
        $this->entityAddress = 'User\Entity\Address';
        $this->service = 'User\Service\User';
        $this->form = 'User\Form\CreateUser';
        $this->route = 'user';
        $this->controller = 'user';
        $this->userMail = 'User\Mail\Mail';
    }

    public function addAction()
    {
        //Usando factories de getFormElementConfig() no module.php
        $formManager = $this->getServiceLocator()
            ->get('FormElementManager');
        $form = $formManager->get($this->form);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $entity = $this->getServiceLocator()
                ->get($this->entityAddress);
            $form->bind($entity);

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $service = $this->getServiceLocator()
                    ->get($this->service);
                //Instância class Base\Mail\Mail
                $mail = $this->getServiceLocator()
                    ->get($this->userMail);

                try {
                    $entity = $service->insert($form->getData(), $mail);

                    $this->flashMessenger()->addSuccessMessage("{$entity->getUser()->getName()} cadastrado com sucesso");
                    return $this->redirect()->toRoute($this->route, array('controller' => $this->controller, 'action' => 'index'));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao inserir o usuário" . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route,
                            array('controller' => $this->controller));
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller
        ));
    }

    public function activateAction()
    {
        $activationKey = $this->params()
            ->fromRoute('key');

        $service = $this->getServiceLocator()
            ->get($this->service);

        if ($result = $service->activate($activationKey))
            return new ViewModel(array(
                'user' => $result
            ));
    }

    public function editAction()
    {
        if (!$id = $this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
        }

        $formManager = $this->getServiceLocator()->get('FormElementManager');
        $form = $formManager->get($this->form);

        try {
            $entityAddress = $this->getEm()
                ->getRepository($this->entityAddress)
                ->findOneByUser($id);

            //Seta Entity com valores setados no form
            $form->bind($entityAddress);

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            //Se passwd estiver em branco remove passwd daatualização
            if ($form->get('address')->get('user')->get('passwd')->getValue() == '') {
                $filter = $form->getInputFilter()->get('address')->get('user');
                $filter->remove('passwd');
                $filter->remove('confirmation');
            }

            if ($form->isValid()) {
                $service = $this->getServiceLocator()
                    ->get($this->service);

                //Try update table
                try {
                    $entity = $service->update($form->getData());

                    $this->flashMessenger()->addSuccessMessage("{$entity->getUser()->getName()} editado com sucesso");

                    return $this->redirect()
                        ->toRoute($this->route, array('controller' => $this->controller));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('Ops! Um problema ocorreu ao editar ' . $e->getMessage());

                    return $this->redirect()
                        ->toRoute($this->route, array('controller' => $this->controller));
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'id' => $id,
            'controller' => $this->controller
        ));
    }

    public function detailAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()->get($this->service);
        try {
            $entityUser = $service->getData($this->entity, $id);

            $entityAddress = $this->getEm()
                ->getRepository($this->entityAddress)
                ->findOneByUser($entityUser);
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Parece que esse ID não existe.');

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        return new ViewModel(array(
            'controller' => $this->controller,
            'detail' => $entityAddress
        ));
    }
}
