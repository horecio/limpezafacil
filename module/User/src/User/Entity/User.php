<?php
namespace User\Entity;

use Base\Entity\AbstractEntity;
use Base\Entity\EntityAwareInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_usuario_perfil_idx", columns={"perfil_id"})})
 * @ORM\Entity(repositoryClass="User\Entity\Repository\User")
 * @ORM\HaslifecycleCallbacks
 */
class User extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=false)
     */
    private $dataNascimento;

    /**
     * @var string
     *
     * @ORM\Column(name="passwd", type="string", length=255, nullable=false)
     */
    private $passwd;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=14, nullable=false)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="operadora", type="string", length=45, nullable=true)
     */
    private $operadora;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=45, nullable=true)
     */
    private $celular;

    /**
     * @var string
     *
     * @ORM\Column(name="fixo", type="string", length=45, nullable=true)
     */
    private $fixo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="activation_key", type="string", length=255, nullable=true)
     */
    private $activationKey;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil", type="string", length=255, nullable=false)
     */
    private $perfil = 'Cliente';


    function __construct()
    {
        $this->setCreatedAt();
        $this->setUpdatedAt();
        $this->setStatus(false);
        //Gera salt
        $this->setSalt(base64_encode(\Zend\Math\Rand::getBytes(16, true)));
        //Gera ActivationKey que é o email concatenado com o salt
        $this->setActivationKey(md5($this->getEmail() . $this->getSalt()));
    }

    /**
     * @internal param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getDateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @ORM\PreUpdate
     * @internal param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt()
    {
            $this->updatedAt = $this->getDateTime();
            return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \DateTime $dataNascimento
     */
    public function setDataNascimento($dataNascimento)
    {
        $this->dataNascimento = $dataNascimento;
    }

    /**
     * @return \DateTime
     */
    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    /**
     * @param string $passwd
     * @return User
     */
    public function setPasswd($passwd)
    {
        $this->passwd = $this->encryptPasswd($passwd);
        return $this;
    }

    public function encryptPasswd($passwd)
    {
        $options = array(
            'salt' => $this->getSalt()
        );
        return password_hash($passwd, PASSWORD_DEFAULT, $options);
    }

    /**
     * @return string
     */
    public function getPasswd()
    {
        return $this->passwd;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param boolean $status
     * @throws \InvalidArgumentException
     * @return User
     */
    public function setStatus($status)
    {
        if (is_bool($status)) {
            $this->status = $status;
            return $this;
        }
        throw new \InvalidArgumentException('Status deve ser boolean');
    }

    /**
     * @param $activationKey
     * @internal param string $activattionKey
     * @return User
     */
    public function setActivationKey($activationKey)
    {
        $this->activationKey = $activationKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivationKey()
    {
        return $this->activationKey;
    }

    /**
     * @param string $operadora
     * @return User
     */
    public function setOperadora($operadora)
    {
        $this->operadora = $operadora;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperadora()
    {
        return $this->operadora;
    }

    /**
     * @param string $celular
     * @return User
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
        return $this;
    }

    /**
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param string $cpf
     * @return User
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $fixo
     * @return User
     */
    public function setFixo($fixo)
    {
        $this->fixo = $fixo;
        return $this;
    }

    /**
     * @return string
     */
    public function getFixo()
    {
        return $this->fixo;
    }

    /**
     * @param string $perfil
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;
    }

    /**
     * @return string
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}
