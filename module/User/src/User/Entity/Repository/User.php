<?php
namespace User\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class User extends EntityRepository
{
    public function findByEmailAndPasswd($email, $passwd)
    {
        $user = $this->findOneByEmail($email);

        if($user) {
            $hashSenha = $user->encryptPasswd($passwd);

            if($hashSenha == $user->getPasswd())
                return $user;

            //senha não confere
            return 'pass';
        }
        //usuário não existe
        return 'user';

    }
} 