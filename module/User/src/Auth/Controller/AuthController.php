<?php
namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AuthController extends AbstractActionController
{
    protected $authAdapter;
    protected $route;
    protected $controller;
    protected $form;

    function __construct()
    {
        $this->authAdapter = 'Auth\Adapter\Authentication';
        $this->controller = 'auth';
        $this->route = 'user-auth';
        $this->form = 'Auth\Form\Login';
    }

    public function indexAction()
    {
        $auth = $this->getServiceLocator()
            ->get('Authentication');

        //Se estiver Autenticado redireciona para Perfil
        if($auth->hasIdentity())
            return $this->redirect()
                ->toRoute('user-perfil');

        $form = $this->getServiceLocator()
            ->get($this->form);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();

                //Criar sessão 'User' para usuário autenticado
                $sessionStorage = $this->getServiceLocator()
                    ->get('Session');
                $auth = $this->getServiceLocator()
                    ->get('Auth');
                $auth->setStorage($sessionStorage);

                //Seta login e password no AuthAdapter
                $authAdapter = $this->getServiceLocator()
                    ->get('Auth\Adapter\Authentication');
                $authAdapter->setUserName($data['email'])
                    ->setPasswd($data['passwd']);
                
                try{
                //faz Autenticação
                $result = $auth->authenticate($authAdapter);
                }catch (\Exception $e){
                    $this->flashMessenger()->addErrorMessage('Ops! Ocorreu algum poblema ao se conectar com o banco. Tente novamente! ' . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route,
                            array('controller' => $this->controller));
                }

                //Se login e senha estiverem corretos
                if ($result->isValid()) {
                    //Se Status for false
                    if (!$result->getIdentity()['user']->getStatus()) {
                        $auth->clearIdentity();

                        $this->flashMessenger()->addErrorMessage("Usuário não está ativo. Verifique seu Email.");
                        return $this->redirect()
                            ->toRoute($this->route);
                    }

                    //Se Status for true
                    $sessionStorage->write($auth->getIdentity(), null);

                    //Redireciona para perfil do usuário
                    return $this->redirect()
                        ->toRoute('user-perfil', array('controller' => 'perfil'));
                }

                if ($result->getCode() == -3){
                    $this->flashMessenger()->addErrorMessage("Login ou senha errados.");

                    return $this->redirect()
                        ->toRoute($this->route);
                }

                //User não existe
                if ($result->getCode() == -1) {
                    $route = 'user/add';

                    $this->flashMessenger()
                        ->addErrorMessage(
                            'Usuário não cadastrado. Quer se Cadastrar?. <a href="' . $route . '">Clique aqui</a>'
                        );

                    return $this->redirect()
                        ->toRoute($this->route);
                }
            }
        }

        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller
        ));
    }

    public function logoutAction()
    {
        $auth = $this->getServiceLocator()
            ->get('Authentication');
        $auth->clearIdentity();

        $this->flashMessenger()->addSuccessMessage("Você fez não está mais logado.");

        //Redireciona para página de login
        return $this->redirect()->toRoute($this->route);
    }
}