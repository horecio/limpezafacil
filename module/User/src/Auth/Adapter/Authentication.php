<?php
namespace Auth\Adapter;

use Doctrine\ORM\EntityManager;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class Authentication implements AdapterInterface
{
    private $em;
    private $username;
    private $passwd;
    private $entity;

    public function __construct(EntityManager $em, $entity)
    {
        $this->em = $em;
        $this->entity = $entity;
    }

    public function authenticate()
    {
        $user = $this->getEm()
            ->getRepository($this->getEntity())
            ->findByEmailAndPasswd(
                $this->getUsername(),
                $this->getPasswd()
            );

        $entity = $this->getEntity();
        if ($user instanceof $entity) {
            if ($user->getStatus())
                return new Result(Result::SUCCESS, array('user' => $user), array('OK'));

            return new Result(Result::SUCCESS, array('user' => $user), array('Usuário não confirmou Email'));
        }
        //usuário existe e pass incorreto
        if ($user == 'pass')
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array('Usuário ou Senha Inválido'));
        //usuário não encontrado
        if ($user == 'user')
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, array('Usuário não cadastrado'));
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $passwd
     * @return Authentication
     */
    public function setPasswd($passwd)
    {
        $this->passwd = $passwd;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswd()
    {
        return $this->passwd;
    }

    /**
     * @param mixed $username
     * @return Authentication
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

}