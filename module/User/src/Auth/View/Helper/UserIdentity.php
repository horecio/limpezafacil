<?php
namespace Auth\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class UserIdentity extends AbstractHelper implements
    ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    //Recebe o nameSpace que é o nome gravado na Storage\Session
    public function __invoke($namespace = null)
    {
        $auth = $this->getServiceLocator()
            ->getServiceLocator()
            ->get('Authentication');

        if (!$auth->hasIdentity())
            return false;

        return $auth->getIdentity();
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return UserIdentity
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;

        return $this;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}