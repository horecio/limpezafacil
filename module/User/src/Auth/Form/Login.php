<?php
namespace Auth\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;

class Login  extends Form
{
    protected $name;
    protected $filter;

    function __construct
    (
        $name,
        InputFilterInterface $filter
    )
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post')
            ->setInputFilter($filter);

        $this->add(array(
                'name' => 'email',
                'type' => 'Text',
                'options' => array(),
                'attributes' => array(
                    'placeholder' => 'EMAIL',
                    'class' => 'form-login',
                )
            )
        );
        $this->add(array(
            'name' => 'passwd',
            'type' => 'Password',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'SENHA',
                'class' => 'form-login',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));
        $this->add(array(
            'name' => 'submit',
            'type'=>'Submit',
            'attributes' => array(
                'value' => 'Entrar',
                'id' => 'sub-login',
                'class' => 'waves-effect waves-button waves-light waves-float btn'
            )
        ));
    }
}
