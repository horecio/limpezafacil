<?php
namespace Auth\Form\Filter;

use Zend\InputFilter\InputFilter;

class Login extends InputFilter
{
    function __construct()
    {
        $this->add(array(
            'name' => 'email',
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Não pode estar em branco',

                        )
                    ),
                )
            )
        ));
        $this->add(array(
            'name' => 'passwd',
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Não pode estar em branco',

                        )
                    ),
                )
            )
        ));
    }
}