<?php
namespace Perfil\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Address extends AbstractService
{
    function __construct(EntityManager $entityManager = null)
    {
        parent::__construct($entityManager);
        $this->entity = 'User\Entity\Address';
    }

    public function insert($entity)
    {
        //Seta false no Endereço existente
        $entityAddress = $this->getEm()
            ->getRepository($this->entity)
            ->findOneByUser($entity->getUser());
        $entityAddress->setIsEntrega(false);

        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }

    public function delete($id)
    {
        $id = (int) $id;
        if (!is_int($id)) {
            $messageException = 'Id deve ser um número inteiro';
            throw new \InvalidArgumentException($messageException);
        }

        if ($id <= 0) {
            $messageException = 'Id deve ser maior do que zero';
            throw new \OutOfBoundsException($messageException);
        }

        $entity = $this->getEm()->getReference($this->entity, $id);

        //Seta o Endereço pessoal como entrega
        $entityAddress = $this->getEm()
            ->getRepository($this->entity)
            ->findOneByUser($entity->getUser());
        $entityAddress->setIsEntrega(true);

        $this->update($entity);

        $id = parent::delete($id);

        return $id;
    }
}