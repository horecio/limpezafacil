<?php
namespace Perfil\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Perfil extends AbstractService
{
    function __construct(EntityManager $entityManager = null)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param $entity string
     * @param $search string
     * @param $id int | $id reference Entity | $id string
     * @return \Base\Entity\AbstractEntity
     */
    public function findOneBy($entity, $search, $id)
    {
        ucfirst(strtolower($search));
        $find = 'findOneBy' . $search;

        return $this->getEm()
            ->getRepository($entity)
            ->$find($id);
    }
}