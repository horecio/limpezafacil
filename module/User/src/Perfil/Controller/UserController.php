<?php
namespace Perfil\Controller;

use Base\Controller\AbstractController;

class UserController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'User\Entity\Address';
        $this->service = 'Perfil\Service\Address';
        $this->form = 'Perfil\Form\Address';
        $this->controller = 'address-perfil';
        $this->route = 'user-perfil/address';
    }

    public function enableAction()
    {
        $this->setStatusCode(404);
    }

    public function disableAction()
    {
        $this->setStatusCode(404);
    }

    public function detailAction()
    {
        $this->setStatusCode(404);
    }
}