<?php
namespace Perfil\Controller;

use Base\Controller\AbstractController;

class AddressController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'User\Entity\Address';
        $this->service = 'Perfil\Service\Address';
        $this->form = 'Perfil\Form\Address';
        $this->controller = 'address-perfil';
        $this->route = 'user-perfil';
    }

    public function enableAction()
    {
        $this->setStatusCode(404);
    }

    public function disableAction()
    {
        $this->setStatusCode(404);
    }

    public function detailAction()
    {
        $this->setStatusCode(404);
    }
}