<?php
namespace Perfil\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PerfilController extends AbstractActionController
{
    private $controller;
    private $route;
    private $service;
    private $em;

    function __construct()
    {
        $this->controller = 'perfil';
        $this->route = 'user-perfil';
        $this->service = 'Perfil\Service\Perfil';
    }

    public function indexAction()
    {
        $auth = $this->getServiceLocator()
            ->get('Authentication');
        $user = $auth->getIdentity()['user'];

        $service = $this->getServiceLocator()
            ->get($this->service);

        //try
        $addressUser = $this->getEm()
            ->getRepository('User\Entity\Address')
            ->findBy(array('user' => $user->getId()));

        return new ViewModel(array(
            'address' => $addressUser,
            'user' => $addressUser[0]->getUser(),
            'route' => $this->route
        ));
    }

    /**
     * @return EntityManager
     */
    protected function getEm()
    {
        if (null === $this->em)
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        return $this->em;
    }
}