<?php
namespace Perfil\Form\Filter;

use Zend\InputFilter\InputFilter;

class Address extends InputFilter
{
    function __construct()
    {
        $this->add(array(
            'name' => 'logradouro',
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));
    }
}