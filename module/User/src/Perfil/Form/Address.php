<?php

namespace Perfil\Form;

use Base\Entity\EntityAwareInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class Address extends Form
{
    protected $name;
    protected $hydrator;
    protected $entity;
    protected $filter;

    public function __construct
    (
        $name,
        AbstractHydrator $hydrator,
        EntityAwareInterface $entity,
        InputFilterInterface $filter
    )
    {
        parent::__construct($name);

        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setInputFilter($filter)
            ->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'logradouro',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'ENDEREÇO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'complemento',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'COMPLEMENTO (Ex.: Quadra..., Portão branco..., Em frente a...)',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cep',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'CEP',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'bairro',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'BAIRRO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'numero',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'N°',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cidade',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'value' => 'GURUPI',
                'class' => 'form-control',
                'disabled' => 'disable'
            ),
        ));
        $this->add(array(
            'name' => 'estado',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'value' => 'TO',
                'class' => 'form-control',
                'disabled' => 'disable'
            ),
        ));
        $this->add(array(
            'name' => 'pais',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'value' => 'Brasil',
                'class' => 'form-control',
                'disabled' => 'disable'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn-success form-control'
            )
        ));
    }
}
