<?php
namespace Acl\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class Role extends EntityRepository
{
    public function fetchNames(){
        $entities = $this->findAll();
        $array = array();
        foreach ($entities as $entity){
            $array[$entity->getName()] = $entity->getName();
        }

        return $array;
    }
} 