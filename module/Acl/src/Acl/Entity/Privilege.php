<?php
namespace Acl\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AclPrivileges
 *
 * @ORM\Table(name="acl_privileges", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_acl_privilege_acl_roles1_idx", columns={"acl_roles_id"}), @ORM\Index(name="fk_acl_privilege_acl_resource1_idx", columns={"acl_resources_id"})})
 * @ORM\Entity
 * @ORM\HaslifecycleCallbacks
 */
class Privilege extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var Resource
     *
     * @ORM\OneToOne(targetEntity="Acl\Entity\Resource")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="acl_resources_id", referencedColumnName="id")
     * })
     */
    private $resource;

    /**
     * @var Role
     *
     * @ORM\OneToOne(targetEntity="Acl\Entity\Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="acl_roles_id", referencedColumnName="id")
     * })
     */
    private $role;

    function __construct()
    {
        $this->setCreatedAt();
        $this->setUpdatedAt();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Privilege
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @internal param \DateTime $createdAt
     * @return Privilege
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\preUpdate
     * Set updatedAt
     *
     * @internal param string $updatedAt
     * @return Privilege
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set Resource
     *
     * @param \Acl\Entity\Resource $resource
     * @return Privilege
     */
    public function setResource(Resource $resource= null)
    {
        $this->resource= $resource;

        return $this;
    }

    /**
     * Get Resource
     *
     * @return Resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Set Role
     *
     * @param Role $role
     * @return Privilege
     */
    public function setRole(Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get Role
     *
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }
}
