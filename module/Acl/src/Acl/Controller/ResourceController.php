<?php
namespace Acl\Controller;

use Base\Controller\AbstractController;

class ResourceController extends AbstractController
{
    public function __construct()
    {
        $this->entity = 'Acl\Entity\Resource';
        $this->service = 'Acl\Service\Resource';
        $this->form = 'Acl\Form\Resource';
        $this->controller = 'resource';
        $this->route = 'acl-resource/default';
    }

    public function enableAction()
    {
        $this->setStatusCode(404);
    }

    public function disableAction()
    {
        $this->setStatusCode(404);
    }

    public function detailAction()
    {
        $this->setStatusCode(404);
    }
}
