<?php
namespace Acl\Controller;

use Base\Controller\AbstractController;

class PrivilegeController extends AbstractController
{
    public function __construct()
    {
        $this->entity = 'Acl\Entity\Privilege';
        $this->service = 'Acl\Service\Privilege';
        $this->form = 'Acl\Form\Privilege';
        $this->controller = 'privilege';
        $this->route = 'acl-privilege/default';
    }

    public function enableAction()
    {
        $this->setStatusCode(404);
    }

    public function disableAction()
    {
        $this->setStatusCode(404);
    }

    public function detailAction()
    {
        $this->setStatusCode(404);
    }
}
