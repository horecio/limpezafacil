<?php
namespace Acl\Controller;

use Base\Controller\AbstractController;

class RoleController extends AbstractController
{
    public function __construct()
    {
        $this->entity = 'Acl\Entity\Role';
        $this->service = 'Acl\Service\Role';
        $this->form = 'Acl\Form\Role';
        $this->controller = 'role';
        $this->route = 'acl-role/default';
    }

    public function enableAction()
    {
        $this->setStatusCode(404);
    }

    public function disableAction()
    {
        $this->setStatusCode(404);
    }

    public function detailAction()
    {
        $this->setStatusCode(404);
    }

}
