<?php
namespace Acl\Form\Filter;

use Zend\InputFilter\InputFilter;

class Role extends InputFilter
{
    function __construct()
    {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
            )
        ));
        $this->add(array(
            'name' => 'parent',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
            )
        ));
    }
}