<?php

namespace Acl\Form;

use Base\Entity\EntityAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class Privilege extends Form implements ObjectManagerAwareInterface
{
    protected $name;
    protected $hydrator;
    protected $entity;
    protected $filter;
    private $objectManager;

    public function __construct
    (
        $name,
        AbstractHydrator $hydrator,
        EntityAwareInterface $entity,
        InputFilterInterface $filter,
        ObjectManager $objectManager
    )
    {
        parent::__construct($name);
        $this->setObjectManager($objectManager);

        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setInputFilter($filter)
            ->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'role',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'empty_option' => 'Escolha o tipo de Usuário',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Acl\Entity\Role',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'resource',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'empty_option' => 'Escolha o Recurso',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Acl\Entity\Resource',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'NOME DO PRIVILÉGIO',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn-success form-control'
            )
        ));
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    public function getObjectManager()
    {
        return $this->objectManager;

    }
}
