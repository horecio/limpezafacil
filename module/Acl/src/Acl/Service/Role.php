<?php
namespace Acl\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Role extends AbstractService
{
    function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = 'Acl\Entity\Role';
    }
}