<?php
namespace Acl\Permissions;

use Zend\Permissions\Acl\Acl as AclClass;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;

class Acl extends AclClass
{
    protected $roles;
    protected $resources;
    protected $privileges;

    function __construct(array $roles, array $resources, array $privileges)
    {
        $this->privileges = $privileges;
        $this->resources = $resources;
        $this->roles = $roles;

        $this->loadRoles();
        $this->loadResources();
        $this->loadPrivileges();
    }
    protected function loadRoles()
    {
        //Verifica todas as Roles
        foreach ($this->roles as $role) {
            //Se tiver pai
            if ($role->getParent()){;
                $this->addRole(
                    new GenericRole($role->getName()),
                    new GenericRole($role->getParent()->getName())
                    );
            }
            else {
                //Se não tiver pai passa somente o nome
                $this->addRole(new GenericRole($role->getName()));
            }

            //Se role for Admin dá perimissão pra tudo
            if ($role->getIsAdmin())
                $this->allow(
                    $role->getName(),
                    array(),
                    array()
                );
        }
    }
    protected function loadResources()
    {
        //Carrega todos os Resources
        foreach($this->resources as $resource) {
            $this->addResource(
                new GenericResource($resource->getName())
            );
        }
    }
    protected function loadPrivileges()
    {
        foreach($this->privileges as $privilege) {
            $this->allow(
                $privilege->getRole()->getName(),
                $privilege->getResource()->getName(),
                $privilege->getName()
            );
        }
    }


}