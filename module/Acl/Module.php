<?php
namespace Acl;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\View\HelperPluginManager;

class Module implements
    AutoloaderProviderInterface,
    ServiceProviderInterface,
    ViewHelperProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return include __DIR__ . '/config/service.config.php';
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                //Add ACL no view Helper Navigation
                'navigation' => function (HelperPluginManager $pm) {
                        $navigation = $pm->get('Zend\View\Helper\Navigation');

                        $auth = $pm->getServiceLocator()
                            ->get('Authentication');

                        //se não estiver autentticado retorna navigation sem Acl
                        if (!$auth->hasIdentity())
                            return $navigation;

                        $acl = $pm->getServiceLocator()
                            ->get('Acl\Permissions\Acl');

                        $role = $auth->getIdentity()['user']
                            ->getPerfil();
                        $navigation->setAcl($acl)
                            ->setRole($role);

                        return $navigation;
                    }
            ),
        );
    }
}
