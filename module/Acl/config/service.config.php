<?php

namespace Acl;

use Acl\Form\Privilege as PrivilegeForm;
use Acl\Form\Resource as ResourceForm;
use Acl\Form\Role as RoleForm;
use Acl\Service\Role as RoleService;
use Acl\Service\Resource as ResourceService;
use Acl\Service\Privilege as PrivilegeService;
use Base\Hydrator\Strategy\EntityStrategy;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Acl\Permissions\Acl;

return array(
        'factories' => array(
            //Role
            'Acl\Form\Role' => function (ServiceLocatorInterface $sm) {
                    $hydrator = new ClassMethods(false);
                    //Covert string em Entity
                    $hydrator->addStrategy('parent',
                        new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Acl\Entity\Role')
                    );

                    return new RoleForm(
                        'role',
                        $hydrator,
                        $sm->get('Acl\Entity\Role'),
                        $sm->get('Acl\Form\Filter\Role'),
                        $sm->get('Doctrine\ORM\EntityManager')
                    );
                },
            'Acl\Service\Role' => function (ServiceLocatorInterface $sm) {
                    return new RoleService(
                        $sm->get('Doctrine\ORM\EntityManager')
                    );
                },
            //Resource
            'Acl\Form\Resource' => function ( ServiceLocatorInterface $sm) {
                    $hydrator = new ClassMethods(false);
                    return new ResourceForm(
                        'resource',
                        $hydrator,
                        $sm->get('Acl\Entity\Resource'),
                        $sm->get('Acl\Form\Filter\Resource')
                    );
                },
            'Acl\Service\Resource' => function ( ServiceLocatorInterface $sm) {
                    return new ResourceService(
                        $sm->get('Doctrine\ORM\EntityManager')
                    );
                },
            //Privilege
            'Acl\Form\Privilege' => function ($sm) {
                    $hydrator = new ClassMethods(false);
                    $hydrator->addStrategy('role',
                        new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Acl\Entity\Role')
                    );
                    $hydrator->addStrategy('resource',
                        new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Acl\Entity\Resource')
                    );

                    return new PrivilegeForm(
                        'privilege',
                        $hydrator,
                        $sm->get('Acl\Entity\Privilege'),
                        $sm->get('Acl\Form\Filter\Privilege'),
                        $sm->get('Doctrine\ORM\EntityManager')
                    );
                },
            'Acl\Service\Privilege' => function (ServiceLocatorInterface $sm) {
                    return new PrivilegeService(
                        $sm->get('Doctrine\ORM\EntityManager')
                    );
                },
            //Permissions ACL
            'Acl\Permissions\Acl' => function (ServiceLocatorInterface $sm) {
                    $em = $sm->get('Doctrine\ORM\EntityManager');

                    $roles = $em->getRepository('Acl\Entity\Role')
                        ->findAll();
                    $resources = $em->getRepository('Acl\Entity\Resource')
                        ->findAll();
                    $privileges = $em->getRepository('Acl\Entity\Privilege')
                        ->findAll();

                    return new Acl($roles, $resources, $privileges);
                },
        ),
        'invokables' => array(
            //Role
            'Acl\Entity\Role' => 'Acl\Entity\Role',
            'Acl\Form\Filter\Role' => 'Acl\Form\Filter\Role',
            //Resource
            'Acl\Form\Filter\Resource' => 'Acl\Form\Filter\Resource',
            'Acl\Entity\Resource' => 'Acl\Entity\Resource',
            //Privilege
            'Acl\Form\Filter\Privilege' => 'Acl\Form\Filter\Privilege',
            'Acl\Entity\Privilege' => 'Acl\Entity\Privilege',
        )
    );