<?php

namespace Acl;

return array(
    'router' => array(
        'routes' => array(
            'acl-privilege' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/privilegio',
                    'defaults' => array(
                        'controller' => 'privilege',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '\d+',
                            ),
                        ),
                    ),
                ),
            ),
            'acl-role' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/papel',
                    'defaults' => array(
                        'controller' => 'role',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '\d+',
                            ),
                        ),
                    ),
                ),
            ),
            'acl-resource' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/recurso',
                    'defaults' => array(
                        'controller' => 'resource',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '\d+',
                            ),
                        ),
                    ),
                ),
            ),
        )

    ),
    'controllers' => array(
        'invokables' => array(
            'role' => 'Acl\Controller\RoleController',
            'resource' => 'Acl\Controller\ResourceController',
            'privilege' => 'Acl\Controller\PrivilegeController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            //...
        ),
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => 'application_entities'
                ),
            ),
        ),
    ),
);
