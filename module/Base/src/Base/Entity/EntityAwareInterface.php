<?php
namespace Base\Entity;


interface EntityAwareInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @return \DateTime 'now'
     */
    public function getDateTime();

    /**
     * Retorna um array com todos os atributos da entity
     *
     * @return array
     */
    public function toArray();
}