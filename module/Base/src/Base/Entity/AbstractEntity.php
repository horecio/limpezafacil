<?php
namespace Base\Entity;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use Doctrine\ORM\Mapping as ORM;

class AbstractEntity implements EntityAwareInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    protected $hydrator;

    /**
     * @return int Id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime 'now'
     */
    public function getDateTime()
    {
        return new \DateTime('now');
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->getHydrator()
            ->extract($this);
    }

    /**
     * @param AbstractHydrator $hydrator
     */
    public function setHydrator(AbstractHydrator $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * @return mixed
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }


}