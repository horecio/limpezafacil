<?php
/**
 * Created by PhpStorm.
 * User: AMZ Dev
 * Date: 27/09/14
 * Time: 16:42
 */

namespace Base\Navigation\Service;


use Zend\Navigation\Service\AbstractNavigationFactory;

class AdminNavigationFactory extends AbstractNavigationFactory
{
    /**
     * @return string
     */
    protected function getName()
    {
        return 'admin';
    }
}