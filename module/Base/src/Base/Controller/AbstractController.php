<?php
namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

abstract class AbstractController extends AbstractActionController
{
    protected $em;
    protected $entity;
    protected $route;
    protected $controller;
    protected $form;
    protected $service;

    public function indexAction()
    {
        $service = $this->getServiceLocator()->get($this->service);
        try {
            $list = $service->getData($this->entity);

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Ops! Um problema ocorreu ao listar ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }
        return new ViewModel(array(
            'list' => $list,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function addAction()
    {
        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $entity = $this->getServiceLocator()
                ->get($this->entity);
            $form->bind($entity);

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                try {
                    $service->insert($form->getData());

                    $this->flashMessenger()->addSuccessMessage('Cadastrado com sucesso');
                    return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('Ops! Um problema ocorreu ao inserir ' . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route,
                            array('controller' => $this->controller));
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function editAction()
    {
        if (!$id = $this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        $service = $this->getServiceLocator()->get($this->service);

        try {
            $entity =  $service->getData($this->entity, $id);

            $form->bind($entity);
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Parece que esse ID não existe ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                //Tenta atuzalizar table
                try {
                    $service->update($form->getData());

                    $this->flashMessenger()->addSuccessMessage('Atualizado com sucesso');
                    return $this->redirect()
                        ->toRoute(
                            $this->route,
                            array('controller' => $this->controller));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('Ops! Um problema ocorreu ao editar ' . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route,
                            array('controller' => $this->controller));
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'id' => $id,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function deleteAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()
            ->get($this->service);

        try {
            $service->delete($id);
            $this->flashMessenger()->addSuccessMessage('Deletado com sucesso');
            return $this->redirect()
                ->toRoute(
                    $this->route,
                    array('controller' => $this->controller));
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Ops! Algum problema ocorreu ao deletar ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }
    }

    public function enableAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()
            ->get($this->service);

        try {
            $service->enable($id);

            $this->flashMessenger()->addSuccessMessage('Ativado com sucesso');

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Ops! Algum problema ocorreu ao ativar ' . $e->getMessage());

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }
    }

    public function disableAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()
            ->get($this->service);

        try {
            $service->disable($id);
            $this->flashMessenger()->addSuccessMessage('Desativado com sucesso');
            return $this->redirect()
                ->toRoute(
                    $this->route,
                    array('controller' => $this->controller));
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Ops! Algum problema ocorreu ao desativar ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }
    }

    public function detailAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()->get($this->service);
        try {
            $entity = $service->getData($this->entity, $id);

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Parece que esse ID não existe ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }

        return new ViewModel(array(
            'controller' => $this->controller,
            'detail' => $entity,
            'route' => $this->route
        ));
    }

    /**
     * @return EntityManager
     */
    protected function getEm()
    {
        if (null === $this->em)
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        return $this->em;
    }

    public function setStatusCode($value)
    {
        $this->response->setStatusCode($value);
    }
}