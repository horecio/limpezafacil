<?php
namespace Base\Hydrator\Strategy;

use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

class DateTimeStrategy extends DefaultStrategy
{
    /**
     * @param mixed $value
     * @return DateTime no format d/m/Y
     */
    public function extract($value)
    {
        return $value->format('d/m/Y');
    }

    /**
     * {@inheritdoc}
     *
     * Convert a string value into a DateTime object
     */
    public function hydrate($value)
    {
        if (is_string($value)){
            //altera formato de data 01/01/2001 para 2001-01-01
            $pieces = explode("/", $value);
            $montagem = $pieces[2].'-'.$pieces[1].'-'.$pieces[0];

            $value = new \DateTime($montagem);
        }

        return $value;
    }

} 