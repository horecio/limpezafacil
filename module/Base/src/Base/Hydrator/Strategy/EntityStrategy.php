<?php
namespace Base\Hydrator\Strategy;

use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

class EntityStrategy extends DefaultStrategy
{
    protected $em;
    protected $entity;

    function __construct(EntityManager $entityManager, $entity)
    {
        $this->em = $entityManager;
        $this->entity = $entity;
    }

    /**
     * @param mixed $value
     * @return mixed|null|object
     */
    public function hydrate($value)
    {
        if (is_string($value) and !empty($value)){
            //Recebe o id da entity e devolve a referência como Objeto
            $value = $this->em
                ->getReference($this->entity, $value);

            return $value;
        }

        return $value = null;
    }

} 