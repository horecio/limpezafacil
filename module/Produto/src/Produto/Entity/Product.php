<?php

namespace Produto\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;

/**
 * Product
 *
 * @ORM\Table(name="product", indexes={@ORM\Index(name="fk_produto_usuario1_idx", columns={"usuario_id"}), @ORM\Index(name="fk_produto_categoria1_idx", columns={"categoria_id"}), @ORM\Index(name="fk_produto_tipo_produto1_idx", columns={"unidade_id"}), @ORM\Index(name="fk_produto_tipo_produto2_idx", columns={"tipo_id"})})
 * @ORM\Entity
 * @ORM\HaslifecycleCallbacks
 */
class Product extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="custo", type="float", precision=9, scale=2, nullable=false)
     */
    private $custo;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float", precision=9, scale=2, nullable=false)
     */
    private $percent;

    /**
     * @var float
     *
     * @ORM\Column(name="sale_price", type="float", precision=9, scale=2, nullable=false)
     */
    private $salePrice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="qtde_unity", type="float", precision=9, scale=2, nullable=false)
     */
    private $qtdeUnity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Produto\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * })
     */
    private $categoria;

    /**
     * @var Unity
     *
     * @ORM\ManyToOne(targetEntity="Produto\Entity\Unity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unidade_id", referencedColumnName="id")
     * })
     */
    private $unidade;

    /**
     * @var ProductType
     *
     * @ORM\ManyToOne(targetEntity="Produto\Entity\ProductType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     * })
     */
    private $tipo;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    function __construct()
    {
        $this->setCreatedAt();
        $this->setUpdatedAt();
        $this->setStatus(true);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set custo
     *
     * @param float $custo
     * @return Product
     */
    public function setCusto($custo)
    {
        $this->custo = $custo;

        return $this;
    }

    /**
     * Get custo
     *
     * @return float 
     */
    public function getCusto()
    {
        return $this->custo;
    }

    /**
     * @param float $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set salePrice
     *
     * @param float $salePrice
     * @return Product
     */
    public function setSalePrice($salePrice)
    {
        $this->salePrice = $salePrice;

        return $this;
    }

    /**
     * Get salePrice
     *
     * @return float 
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set qtdeUnity
     *
     * @param float $qtdeUnity
     * @return Product
     */
    public function setQtdeUnity($qtdeUnity)
    {
        $this->qtdeUnity = $qtdeUnity;

        return $this;
    }

    /**
     * get qtdeUnity
     *
     * @return float
     */
    public function getQtdeUnity()
    {
        return $this->qtdeUnity;
    }

    /**
     * Set createdAt
     *
     * @return Product
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * Set updatedAt
     *
     * @return Product
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set categoria
     *
     * @param Category $categoria
     * @return Product
     */
    public function setCategoria(Category $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return Category
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set unidade
     *
     * @param Unity $unidade
     * @return Product
     */
    public function setUnidade(Unity $unidade = null)
    {
        $this->unidade = $unidade;

        return $this;
    }

    /**
     * Get unidade
     *
     * @return Unity
     */
    public function getUnidade()
    {
        return $this->unidade;
    }

    /**
     * Set tipo
     *
     * @param ProductType $tipo
     * @return Product
     */
    public function setTipo(ProductType $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return ProductType
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Product
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
