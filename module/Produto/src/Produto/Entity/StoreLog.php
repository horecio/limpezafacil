<?php

namespace Produto\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * StoreLog
 *
 * @ORM\Table(name="store_log", indexes={@ORM\Index(name="fk_estoque_log_estoque1_idx", columns={"estoque_id"})})
 * @ORM\Entity
 */
class StoreLog extends AbstractEntity
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="qtde", type="integer", nullable=false)
     */
    private $qtde;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tipo", type="boolean", nullable=false)
     */
    private $tipo;

    /**
     * @var \Produto\Entity\Store
     *
     * @ORM\ManyToOne(targetEntity="Produto\Entity\Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estoque_id", referencedColumnName="id")
     * })
     */
    private $estoque;

    function __construct()
    {
        $this->setCreatedAt();
    }

    /**
     * Set createdAt
     *
     * @return StoreLog
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set qtde
     *
     * @param integer $qtde
     * @return StoreLog
     */
    public function setQtde($qtde)
    {
        $this->qtde = $qtde;

        return $this;
    }

    /**
     * Get qtde
     *
     * @return integer 
     */
    public function getQtde()
    {
        return $this->qtde;
    }

    /**
     * Set tipo
     *
     * @param boolean $tipo
     * @return StoreLog
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return boolean 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set estoque
     *
     * @param \Produto\Entity\Store $estoque
     * @return StoreLog
     */
    public function setEstoque(\Produto\Entity\Store $estoque = null)
    {
        $this->estoque = $estoque;

        return $this;
    }

    /**
     * Get estoque
     *
     * @return \Produto\Entity\Store 
     */
    public function getEstoque()
    {
        return $this->estoque;
    }
}
