<?php

namespace Produto\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Store
 *
 * @ORM\Table(name="store", indexes={@ORM\Index(name="fk_estoque_produto1_idx", columns={"produto_id"})})
 * @ORM\Entity
 * @ORM\HaslifecycleCallbacks
 */
class Store extends AbstractEntity
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="qtde", type="integer", nullable=false)
     */
    private $qtde;

    /**
     * @var \Produto\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Produto\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     * })
     */
    private $produto;

    function __construct()
    {
        $this->setCreatedAt();
        $this->setUpdatedAt();
    }

    /**
     * Set createdAt
     *
     * @return Store
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * Set updatedAt
     *
     * @return Store
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = $this->getDateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set qtde
     *
     * @param integer $qtde
     * @return Store
     */
    public function setQtde($qtde)
    {
        $this->qtde = $qtde;

        return $this;
    }

    /**
     * Get qtde
     *
     * @return integer 
     */
    public function getQtde()
    {
        return $this->qtde;
    }

    /**
     * Set produto
     *
     * @param \Produto\Entity\Product $produto
     * @return Store
     */
    public function setProduto(\Produto\Entity\Product $produto = null)
    {
        $this->produto = $produto;

        return $this;
    }

    /**
     * Get produto
     *
     * @return \Produto\Entity\Product 
     */
    public function getProduto()
    {
        return $this->produto;
    }
}
