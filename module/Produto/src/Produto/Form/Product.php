<?php

namespace Produto\Form;

use Base\Entity\EntityAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class Product extends Form implements ObjectManagerAwareInterface
{
    protected $name;
    protected $hydrator;
    protected $entity;
    protected $filter;
    private $objectManager;

    public function __construct
    (
        $name,
        AbstractHydrator $hydrator,
        EntityAwareInterface $entity,
        InputFilterInterface $filter,
        ObjectManager $objectManager
    )
    {
        parent::__construct($name);
        $this->setObjectManager($objectManager);

        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setInputFilter($filter)
            ->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'NOME DO PRODUTO',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'TextArea',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'DESCRIÇÂO',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'qtdeUnity',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'QUANTIDADE DA UNIDADE',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'unidade',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'empty_option' => 'Escolha a Unidade de Medida',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Produto\Entity\Unity',
                'property' => 'shortName'
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'custo',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'CUSTO DO PRODUTO',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'percent',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'MARGEM DE LUCRO - Ex.: 10, 5.5, etc',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'salePrice',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'PREÇO DE VENDA',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'categoria',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'empty_option' => 'Escolha a categoria',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Produto\Entity\Category',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'tipo',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'empty_option' => 'Escolha o Tipo de Produto',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Produto\Entity\ProductType',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn-success form-control'
            )
        ));
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    public function getObjectManager()
    {
        return $this->objectManager;

    }
}
