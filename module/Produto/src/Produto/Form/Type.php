<?php
namespace Produto\Form;

use Base\Entity\EntityAwareInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class Type extends Form
{
    protected $name;
    protected $hydrator;
    protected $entity;
    protected $filter;

    public function __construct(
        $name,
        AbstractHydrator $hydrator,
        EntityAwareInterface $entity,
        InputFilterInterface $filter
    )
    {
        parent::__construct($name);

        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setInputFilter($filter)
            ->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'NOME (LIQUIDO, EM PÓ...)',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'TextArea',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'DESCRIÇÂO',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            )
        ));
    }

}
