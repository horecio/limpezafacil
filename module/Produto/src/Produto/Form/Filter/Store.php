<?php
namespace Produto\Form\Filter;

use Zend\InputFilter\InputFilter;

class Store extends InputFilter
{
    function __construct()
    {
        $this->add(array(
            'name' => 'qtde',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
            )
        ));
    }
}