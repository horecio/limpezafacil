<?php
namespace Produto\Form\Filter;

use Zend\InputFilter\InputFilter;

class Category extends InputFilter
{
    function __construct()
    {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
            )
        ));
    }
}