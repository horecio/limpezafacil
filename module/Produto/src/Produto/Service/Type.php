<?php
namespace Produto\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Type extends AbstractService
{
    function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = 'Produto\Entity\ProductType';
    }
}