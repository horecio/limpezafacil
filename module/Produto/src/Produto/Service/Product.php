<?php
namespace Produto\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Product
    extends AbstractService
    implements ServiceLocatorAwareInterface
{
    private $serviceLocator;

    function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = 'Produto\Entity\Product';
    }

    public function insert($entity)
    {
        /**
         * get Storate de Usuário Autenticado
         */
        $auth = $this->getServiceLocator()
            ->get('Authentication');

        $userEntity = $this->getEm()
            ->getReference('User\Entity\User', $auth->getidentity()['user']->getId());

        $entity->setUser($userEntity); //seta Usuário Logado na entity

        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}