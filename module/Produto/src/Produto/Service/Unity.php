<?php
namespace Produto\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Unity extends AbstractService
{
    function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = 'Produto\Entity\Unity';
    }
}