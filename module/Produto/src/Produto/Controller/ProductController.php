<?php

namespace Produto\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class ProductController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'Produto\Entity\Product';
        $this->service = 'Produto\Service\Product';
        $this->form = 'Produto\Form\Product';
        $this->controller = 'produto';
        $this->route = 'product';
    }

    public function detailAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()->get($this->service);
        try {
            $entity = $service->getData($this->entity, $id);

            $store = $this->getEm()
                ->getRepository('Produto\Entity\Store')
                ->findOneByProduto($entity);

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Parece que esse ID não existe ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }

        return new ViewModel(array(
            'controller' => $this->controller,
            'detail' => $entity,
            'route' => $this->route,
            'store' => $store
        ));
    }
}