<?php

namespace Produto\Controller;

use Base\Controller\AbstractController;

class UnityController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'Produto\Entity\Unity';
        $this->service = 'Produto\Service\Unity';
        $this->form = 'Produto\Form\Unity';
        $this->controller = 'unidade';
        $this->route = 'unity';
    }
}