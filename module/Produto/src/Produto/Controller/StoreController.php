<?php

namespace Produto\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class StoreController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'Produto\Entity\Store';
        $this->service = 'Produto\Service\Store';
        $this->form = 'Produto\Form\Store';
        $this->controller = 'estoque';
        $this->route = 'store';
    }

    public function addAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $entity = $this->getServiceLocator()
                ->get($this->entity);

            $form = $this->getServiceLocator()
                ->get($this->form);
            $form->bind($entity);

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                try {
                    $service->insert($form->getData());

                    $this->flashMessenger()->addSuccessMessage('Cadastrado com sucesso');

                    return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('Ops! Um problema ocorreu ao inserir ' . $e->getMessage());

                    return $this->redirect()
                        ->toRoute($this->route,
                            array('controller' => $this->controller));
                }
            }
        }
    }

    public function editAction()
    {
        if (!$id = $this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID na Url.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        $service = $this->getServiceLocator()->get($this->service);

        try {
            $entity =  $service->getData($this->entity, $id);

            $form->bind($entity);
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Parece que esse ID não existe ' . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                //Tenta atuzalizar table
                try {
                    $service->update($form->getData());

                    $this->flashMessenger()->addSuccessMessage('Atualizado com sucesso');
                    return $this->redirect()
                        ->toRoute(
                            $this->route,
                            array('controller' => $this->controller));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('Ops! Um problema ocorreu ao editar ' . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route,
                            array('controller' => $this->controller));
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'id' => $id,
            'controller' => $this->controller,
            'route' => $this->route,
        ));
    }

    public function produtoAction()
    {
        if (!$id = $this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Precisa passar o ID do Produto na Url.");

            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $form = $this->getServiceLocator()
            ->get($this->form);

        //Usa o service do Product pois vai utilizar somente o getData()
        $service = $this->getServiceLocator()->get($this->service);

        //Find product with ID passada pro parâmetro
        try {
            $entityProd = $service->getData('Produto\Entity\Product', $id);

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Ops! Produto não encontrado ' . $e->getMessage());

            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }

        return new ViewModel(array(
            'product' => $entityProd,
            'form' => $form,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function enableAction()
    {
        $this->setStatusCode(404);
    }

    public function disableAction()
    {
        $this->setStatusCode(404);
    }

    public function detailAction()
    {
        $this->setStatusCode(404);
    }
}