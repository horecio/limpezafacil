<?php

namespace Produto\Controller;

use Base\Controller\AbstractController;

class TypeController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'Produto\Entity\ProductType';
        $this->service = 'Produto\Service\Type';
        $this->form = 'Produto\Form\Type';
        $this->controller = 'tipo';
        $this->route = 'type';
    }
}