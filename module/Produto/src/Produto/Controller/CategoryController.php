<?php

namespace Produto\Controller;

use Base\Controller\AbstractController;

class CategoryController extends AbstractController
{
    function __construct()
    {
        $this->entity = 'Produto\Entity\Category';
        $this->service = 'Produto\Service\Category';
        $this->form = 'Produto\Form\Category';
        $this->controller = 'categoria';
        $this->route = 'category';
    }
}