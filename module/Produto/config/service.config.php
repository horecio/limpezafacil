<?php

namespace Produto;

use Base\Hydrator\Strategy\EntityStrategy;
use Produto\Form\Category as CategoryForm;
use Produto\Form\Type as TypeForm;
use Produto\Form\Unity as UnityForm;
use Produto\Form\Product as ProductForm;
use Produto\Form\Store as StoreForm;
use Produto\Service\Category as CategoryService;
use Produto\Service\Type as TypeService;
use Produto\Service\Unity as UnityService;
use Produto\Service\Product as ProductService;
use Produto\Service\Store as StoreService;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

return array(
    'factories' => array(
        //Category
        __NAMESPACE__ . '\Form\Category' => function (ServiceLocatorInterface $sm) {
                $hydrator = new ClassMethods(false);

                return new CategoryForm(
                    'category',
                    $hydrator,
                    $sm->get('Produto\Entity\Category'),
                    $sm->get('Produto\Form\Filter\Category')
                );
            },
        __NAMESPACE__ . '\Service\Category' => function (ServiceLocatorInterface $sm) {
                return new CategoryService(
                    $sm->get('Doctrine\ORM\EntityManager')
                );
            },
        //Type
        __NAMESPACE__ . '\Form\Type' => function (ServiceLocatorInterface $sm) {
                $hydrator = new ClassMethods(false);

                return new TypeForm(
                    'type',
                    $hydrator,
                    $sm->get('Produto\Entity\ProductType'),
                    $sm->get('Produto\Form\Filter\Type')
                );
            },
        __NAMESPACE__ . '\Service\Type' => function (ServiceLocatorInterface $sm) {
                return new TypeService(
                    $sm->get('Doctrine\ORM\EntityManager')
                );
            },
        //Unity
        __NAMESPACE__ . '\Form\Unity' => function (ServiceLocatorInterface $sm) {
                $hydrator = new ClassMethods(false);

                return new UnityForm(
                    'type',
                    $hydrator,
                    $sm->get('Produto\Entity\Unity'),
                    $sm->get('Produto\Form\Filter\Unity')
                );
            },
        __NAMESPACE__ . '\Service\Unity' => function (ServiceLocatorInterface $sm) {
                return new UnityService(
                    $sm->get('Doctrine\ORM\EntityManager')
                );
            },
        //Product
        __NAMESPACE__ . '\Form\Product' => function ($sm) {
                $hydrator = new ClassMethods(false);
                $hydrator->addStrategy('categoria',
                    new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Produto\Entity\Category')
                );
                $hydrator->addStrategy('unidade',
                    new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Produto\Entity\Unity')
                );
                $hydrator->addStrategy('tipo',
                    new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Produto\Entity\ProductType')
                );

                return new ProductForm(
                    'product',
                    $hydrator,
                    $sm->get('Produto\Entity\Product'),
                    $sm->get('Produto\Form\Filter\Product'),
                    $sm->get('Doctrine\ORM\EntityManager')
                );
            },
        __NAMESPACE__ . '\Service\Product' => function (ServiceLocatorInterface $sm) {
                $productService = new ProductService(
                    $sm->get('Doctrine\ORM\EntityManager')
                );
                $productService->setServiceLocator($sm);

                return $productService;
            },
        //Store
        __NAMESPACE__ . '\Form\Store' => function (ServiceLocatorInterface $sm) {
                $hydrator = new ClassMethods(false);
                $hydrator->addStrategy('produto',
                    new EntityStrategy($sm->get('Doctrine\ORM\EntityManager'), 'Produto\Entity\Product')
                );

                return new StoreForm(
                    'store',
                    $hydrator,
                    $sm->get('Produto\Entity\Store'),
                    $sm->get('Produto\Form\Filter\Store')
                );
            },
        __NAMESPACE__ . '\Service\Store' => function (ServiceLocatorInterface $sm) {
                $productService = new StoreService(
                    $sm->get('Doctrine\ORM\EntityManager')
                );

                return $productService;
            },
    ),
    'invokables' => array(
        //Category
        __NAMESPACE__ . '\Form\Filter\Category' => __NAMESPACE__ . '\Form\Filter\Category',
        __NAMESPACE__ . '\Entity\Category' => __NAMESPACE__ . '\Entity\Category',
        //Type
        __NAMESPACE__ . '\Form\Filter\Type' => __NAMESPACE__ . '\Form\Filter\Type',
        __NAMESPACE__ . '\Entity\ProductType' => __NAMESPACE__ . '\Entity\ProductType',
        //Unit
        __NAMESPACE__ . '\Form\Filter\Unity' => __NAMESPACE__ . '\Form\Filter\Unity',
        __NAMESPACE__ . '\Entity\Unity' => __NAMESPACE__ . '\Entity\Unity',
        //Product
        __NAMESPACE__ . '\Form\Filter\Product' => __NAMESPACE__ . '\Form\Filter\Product',
        __NAMESPACE__ . '\Entity\Product' => __NAMESPACE__ . '\Entity\Product',
        //Store
        __NAMESPACE__ . '\Form\Filter\Store' => __NAMESPACE__ . '\Form\Filter\Store',
        __NAMESPACE__ . '\Entity\Store' => __NAMESPACE__ . '\Entity\Store',
    )
);