<?php

namespace Produto;

return array(
    'router' => array(
        'routes' => array(
            'product' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/produto[/:action][/:id]',
                    'defaults' => array(
                        'controller' => 'produto',
                        'action' => 'index',
                    ),
                ),
            ),
            'category' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/categoria[/:action][/:id]',
                    'defaults' => array(
                        'controller' => 'categoria',
                        'action' => 'index',
                    ),
                ),
            ),
            'type' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/tipo[/:action][/:id]',
                    'defaults' => array(
                        'controller' => 'tipo',
                        'action' => 'index',
                    ),
                ),
            ),
            'unity' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/unidade[/:action][/:id]',
                    'defaults' => array(
                        'controller' => 'unidade',
                        'action' => 'index',
                    ),
                ),
            ),
            'store' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/estoque[/:action][/:id]',
                    'defaults' => array(
                        'controller' => 'estoque',
                        'action' => 'index',
                    ),
                ),
            ),
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'produto' => 'Produto\Controller\ProductController',
            'categoria' => 'Produto\Controller\CategoryController',
            'tipo' => 'Produto\Controller\TypeController',
            'unidade' => 'Produto\Controller\UnityController',
            'estoque' => 'Produto\Controller\StoreController'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => 'application_entities'
                ),
            ),
        ),
    ),

);
