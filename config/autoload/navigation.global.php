<?php
return array(
    'navigation' => array(
        'home' => array(
            array(
                'label' => 'Home',
                'route' => 'home',
                'title' => 'Início',
                'module' => 'Application',
                'controller' => 'home',
                'action'     => 'index',
                'order'      => -100

            ),
            array(
                'label' => 'Login',
                'route' => 'user-auth',
                'module' => 'User',
                'controller' => 'auth',
                'action' => 'index',
            ),
            array(
                'label' => 'Cadastro',
                'route' => 'user',
                'module' => 'User',
                'controller' => 'user',
                'action' => 'add',
            )
        ),
        'admin' => array(
            array(
                'label' => 'Perfil',
                'route' => 'user-perfil',
                'module'     => 'User',
                'controller' => 'perfil',
                'action'     => 'index',
                'resource' => 'Perfil',
                'privilege' => 'index',
                'id' => 'child-muted',
                'pages' => array(
                    array(
                        'label' => 'Endereço / editar',
                        'route' => 'user-perfil/address',
                        'action'     => 'edit',
                        'resource' => 'Address-perfil',
                        'privilege' => 'edit',
                    ),
                    array(
                        'label' => 'Endereço / adicionar',
                        'route' => 'user-perfil/address',
                        'action'     => 'add',
                        'resource' => 'Address-perfil',
                        'privilege' => 'add'
                    )
                )
            ),
            array(
                'label' => 'Usuario',
                'route' => 'user',
                'module' => 'User',
                'resource' => 'User',
                'privilege' => 'add',
                'id' => 'child-muted',
                'pages' => array(
                    array(
                        'label' => 'adicionar',
                        'route' => 'user',
                        'action'     => 'add',
                        'resource' => 'User',
                        'privilege' => 'add'
                    ),
                    array(
                        'label' => 'editar',
                        'route' => 'user',
                        'action'     => 'edit',
                        'resource' => 'User',
                        'privilege' => 'edit'
                    ),
                    array(
                        'label' => 'detalhes',
                        'route' => 'user',
                        'action'     => 'detail',
                        'resource' => 'User',
                        'privilege' => 'detail'
                    )
                )
            ),
            array(
                'label' => 'ACL',
                'route' => 'acl-role',
                'module'     => 'Acl',
                'resource' => 'nav:Acl',
                'pages' => array(
                    array(
                        'label' => 'Papel',
                        'route' => 'acl-role',
                        'action'     => 'index',
                        'resource' => 'Role',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'adicionar',
                                'route' => 'acl-role/default',
                                'action'     => 'add',
                                'resource' => 'Role',
                                'privilege' => 'add',
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'acl-role/default',
                                'action'     => 'edit',
                                'resource' => 'Role',
                                'privilege' => 'edit',
                            )
                        )
                    ),
                    array(
                        'label' => 'Recurso',
                        'route' => 'acl-resource',
                        'action'     => 'index',
                        'resource' => 'Resource',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'adicionar',
                                'route' => 'acl-resource/default',
                                'action'     => 'add',
                                'resource' => 'Resource',
                                'privilege' => 'add',
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'acl-resource/default',
                                'action'     => 'edit',
                                'resource' => 'Resource',
                                'privilege' => 'edit',
                            )
                        )
                    ),
                    array(
                        'label' => 'Privilégio',
                        'route' => 'acl-privilege',
                        'action'     => 'index',
                        'resource' => 'Privilege',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'adicionar',
                                'route' => 'acl-privilege/default',
                                'action'     => 'add',
                                'resource' => 'Privilege',
                                'privilege' => 'add',
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'acl-privilege/default',
                                'action'     => 'edit',
                                'resource' => 'Privilege',
                                'privilege' => 'edit',
                            )
                        )
                    )
                )
            ),
            array(
                'label' => 'Produtos',
                'route' => 'product',
                'module'     => 'Produto',
                'resource' => 'nav:Produtos',
                'pages' => array(
                    array(
                        'label' => 'Produto',
                        'route' => 'product',
                        'action' => 'index',
                        'resource' => 'Produto',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'detalhes',
                                'route' => 'product',
                                'action'     => 'detail',
                                'resource' => 'Produto',
                                'privilege' => 'detail'
                            ),
                            array(
                                'label' => 'adicionar',
                                'route' => 'product',
                                'action'     => 'add',
                                'resource' => 'Produto',
                                'privilege' => 'add'
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'product',
                                'action'     => 'edit',
                                'resource' => 'Produto',
                                'privilege' => 'edit'
                            )
                        )
                    ),
                    array(
                        'label' => 'Categoria',
                        'route' => 'category',
                        'action' => 'index',
                        'resource' => 'Categoria',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'detalhes',
                                'route' => 'category',
                                'action'     => 'detail',
                                'resource' => 'Categoria',
                                'privilege' => 'detail',
                            ),
                            array(
                                'label' => 'adicionar',
                                'route' => 'category',
                                'action'     => 'add',
                                'resource' => 'Categoria',
                                'privilege' => 'add',
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'category',
                                'action'     => 'edit',
                                'resource' => 'Categoria',
                                'privilege' => 'edit',
                            )
                        )
                    ),
                    array(
                        'label' => 'Tipo',
                        'route' => 'type',
                        'action' => 'index',
                        'resource' => 'Tipo',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'detalhes',
                                'route' => 'type',
                                'action'     => 'detail',
                                'resource' => 'Tipo',
                                'privilege' => 'detail'
                            ),
                            array(
                                'label' => 'adicionar',
                                'route' => 'type',
                                'action'     => 'add',
                                'resource' => 'Tipo',
                                'privilege' => 'add'
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'type',
                                'action'     => 'edit',
                                'resource' => 'Tipo',
                                'privilege' => 'edit'
                            )
                        )
                    ),
                    array(
                        'label' => 'Unidade',
                        'route' => 'unity',
                        'action' => 'index',
                        'resource' => 'Unidade',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'detalhes',
                                'route' => 'unity',
                                'action'     => 'detail',
                                'resource' => 'Unidade',
                                'privilege' => 'detail'
                            ),
                            array(
                                'label' => 'adicionar',
                                'route' => 'unity',
                                'action'     => 'add',
                                'resource' => 'Unidade',
                                'privilege' => 'add'
                            ),
                            array(
                                'label' => 'editar',
                                'route' => 'unity',
                                'action'     => 'edit',
                                'resource' => 'Unidade',
                                'privilege' => 'edit'
                            )
                        )
                    ),
                    array(
                        'label' => 'Estoque',
                        'route' => 'store',
                        'action' => 'index',
                        'resource' => 'Estoque',
                        'privilege' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'editar',
                                'route' => 'store',
                                'action'     => 'edit',
                                'resource' => 'Estoque',
                                'privilege' => 'edit'
                            ),
                            array(
                                'label' => 'adicionar',
                                'route' => 'store',
                                'action'     => 'produto',
                                'resource' => 'Estoque',
                                'privilege' => 'add'
                            )
                        )
                    )
                )
            ),
            array(
                'label' => 'Logout',
                'route' => 'user-auth/default',
                'module' => 'User',
                'controller' => 'auth',
                'action' => 'logout',
                'resource' => 'Auth',
                'privilege' => 'logout'
            ),
        ),
    ),

    'service_manager' => array(
        'factories' => array(
            'home_nav' => 'Base\Navigation\Service\HomeNavigationFactory',
            'admin_nav' => 'Base\Navigation\Service\AdminNavigationFactory',
        ),
    ),
);
